"""
SPDX-License-Identifier: MIT
Copyright (c) 2018-2022 OpenWG team
"""

#
# Imports
#

# sentry-sdk
import sentry_sdk
from sentry_sdk.integrations.logging import LoggingIntegration
from sentry_sdk.integrations.stdlib import StdlibIntegration
from sentry_sdk.integrations.excepthook import ExcepthookIntegration
from sentry_sdk.integrations.dedupe import DedupeIntegration
from sentry_sdk.integrations.atexit import AtexitIntegration
from sentry_sdk.integrations.threading import ThreadingIntegration

# openwg.bugreports
from .integration_config import OpenWGConfigIntegration
from .integration_filter import OpenWGFilterIntegration
from .integration_mods import OpenWGModsIntegration



#
# Classes
#

class OpenWGBugreportsPython(object):
    def __init__(self, dsn, release, environment):
        sentry_sdk.init(
            auto_enabling_integrations=False,
            auto_session_tracking=False,
            attach_stacktrace=True,
            debug = False,
            default_integrations=False,
            dsn=dsn,
            environment=environment, 
            integrations=[
                # sentry
                AtexitIntegration(),
                DedupeIntegration(),
                ExcepthookIntegration(),
                LoggingIntegration(),
                StdlibIntegration(),            
                ThreadingIntegration(),
                # opewg
                OpenWGConfigIntegration(),
                OpenWGFilterIntegration(),
                OpenWGModsIntegration()
            ],
            release=release,
            send_default_pii=True,
        )
        sentry_sdk.Hub.current.start_session('application')

    def __fini__(self):
        sentry_sdk.Hub.current.end_session()
        sentry_sdk.flush()

    def set_context(self, ctx_key, ctx_val):
        sentry_sdk.set_context(ctx_key, ctx_val)

    def set_tag(self, tag_key, tag_value):
        sentry_sdk.set_tag(tag_key, tag_value)

    def set_user(self, user_ctx):
        sentry_sdk.set_user(user_ctx)
