#
# Imports
#
from .integration_config import OpenWGConfigIntegration

# sentry_sdk
from sentry_sdk.hub import Hub
from sentry_sdk.integrations import Integration
from sentry_sdk.scope import add_global_event_processor


#
# Integration
#

class OpenWGFilterIntegration(Integration):
    identifier = "openwg_filter"

    @staticmethod
    def setup_once():

        @add_global_event_processor
        def processor(event, hint):
            # forward in case of uninitialized config
            config = Hub.current.get_integration(OpenWGConfigIntegration)
            if config is None:
                return event

            # drop event in case of disabled logging
            if config.enabled == False:
                return None

            # forward transactions
            if event.get("type") == "transaction":
                return event

            # forward events without hints
            if hint is None:
                return event

            # forward in case of unregistered integration
            integration = Hub.current.get_integration(OpenWGFilterIntegration)
            if integration is None:
                return event

            # drop ignored log records
            log_record = hint.get("log_record", None)
            if log_record is not None and 'log' in config.ignore_list:
                message = log_record.getMessage()
                for entry in config.ignore_list['log']:
                    if entry in message:
                        return None

            # drop ignored exceptions
            exc_info = hint.get("exc_info", None)
            if exc_info is not None and len(exc_info) >= 2:
                for entry in config.ignore_list['exc']:
                    if entry in exc_info[1]:
                        return None

            return event
