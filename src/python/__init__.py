"""
SPDX-License-Identifier: MIT
Copyright (c) 2018-2022 OpenWG team
"""

#
# Imports
#

#cpython
import logging
import os.path
import platform
import xml.etree.ElementTree as ET

#bigworld/wot
import BigWorld
from constants import CURRENT_REALM
from gui.shared.utils import getPlayerDatabaseID, getPlayerName
from helpers import dependency, getClientLanguage
from skeletons.gui.app_loader import IAppLoader, GuiGlobalSpaceID

#xfw.loader
import xfw_loader.python as loader

# openwg.vfs
import openwg_vfs as vfs

#openwg.bugreports
from .python import OpenWGBugreportsPython



#
# Constants
#

SENTRY_DSN_NATIVE_LESTA = ''
SENTRY_DSN_NATIVE_WG    = ''

SENTRY_DSN_PYTHON_LESTA = 'https://f45918aa51dcf406e5af6b5a8fe666be@sentry.openwg.net/3'
SENTRY_DSN_PYTHON_WG    = 'https://7ab39fe46dc9ab227f603006d47985c4@sentry.openwg.net/2'

XFW_BUGREPORTING_PRIVACY = 'https://privacy.openwg.net/'



#
# Globals
#

g_openwg_bugreports = None

g_xfw_bugreport_initialized = False
g_xfw_bugreport_native = None
g_xfw_bugreport_python = None


#
# Class
#

class OpenWGBugreports(object):

    #
    # Initialize
    #

    def __init__(self):
        self.__inited = False
        self.__logger = logging.getLogger('OpenWG/BugReports')
        self.__native = None
        self.__python = None

        self.__release = self.__release_get()
        self.__environment = self.__environment_get()

        if self.__consent_status():
            self.__python = OpenWGBugreportsPython(SENTRY_DSN_PYTHON_LESTA if loader.get_client_realm() == 'RU' else SENTRY_DSN_PYTHON_WG, self.__release, self.__environment)

        if self.__native is None and self.__python is None:
            return

        self.set_user(self.__context_user_get())

        self.set_context("app", self.__context_app_get())
        self.set_context("culture", self.__context_culture_get())
        self.set_context("device", self.__context_device_get())
        self.set_context("gpu", self.__context_gpu_get())
        self.set_context("os", self.__context_os_get())

        self.__tags_device_set()
        self.__tags_storage_set()
        self.__tags_modpack_set()

        self.__events_subscribe()
        self.__inited = True


    def __fini__(self):
        self.__events_unsubscribe()
        if self.__native is not None:
            self.__native.__fini__()
        if self.__python is not None:
            self.__python.__fini__()


    def is_inited(self):
        return self.__inited


    #
    # Consent
    #

    def __consent_required(self):
        consent_required = False

        realm = loader.get_client_realm()
        if realm == 'RU' or realm == 'CT' or realm == 'ST':
            self.__logger.info("check_consent_required: bugreporting does not require user consent because of RU/CT/ST region. Please add empty 'OPENWG_BUGREPORTS_OPTOUT.txt' file to game root to disable bugreports")
            consent_required = False
        else:
            self.__logger.info("check_consent_required: bugreporting requires user consent. Please add empty 'OPENWG_BUGREPORTS_OPTIN.txt' file to game root to enable bugreports")
            consent_required = True

        return consent_required

    def __consent_status(self):
        consent_required = self.__consent_required()

        if os.path.exists('OPENWG_BUGREPORTS_OPTOUT.txt'):
            self.__logger.info("check_consent_status: bugreporting disabled because of user opt-out")
            return False
        if os.path.exists('OPENWG_BUGREPORTS_OPTIN.txt'):
            self.__logger.info("check_consent_status: bugreporting enabled because of user opt-in")
            return True
        elif os.path.exists('wargaming_qa.conf'):
            self.__logger.info("check_consent_status: bugreporting enabled because of QA mode")
            return True
        elif consent_required:
            self.__logger.info("check_consent_status: bugreporting disabled because user consent was not given")
            return False

        self.__logger.info("check_consent_status: bugreporting enabled because user consent is not required")
        return True


    #
    # Environment
    #

    def __environment_get(self):
        if os.path.exists('wargaming_qa.conf'):
            return 'QA'
        return CURRENT_REALM


    #
    # Release
    #

    def __release_get(self):
        result = loader.WOT_VERSION_FULL.replace(' ', '_')
        
        if os.path.exists('game_info.xml'):
            try:
                result = ET.parse('game_info.xml').getroot().findtext('game/version_name')
            except:
                self.__logger.exception('release_get: failed to process game_info')
        
        return result

    #
    # Context
    #

    def __context_app_get(self):
        return {
            'app_name': 'World of Tanks',
            'app_version': loader.WOT_VERSION_FULL
        }

    def __context_culture_get(self):
        return {
            'locale': getClientLanguage()
        }

    def __context_device_get(self):
        stats = BigWorld.wg_getClientStatistics()
        return {
            'arch': self.__context_device_get_arch(),
            'cpu_description': stats['cpuName'].strip(),
            'memory_size': stats['ramTotal'] * 1024 * 1024,
            'processor_count': stats['cpuCores'],
            'processor_frequency': stats['cpuFreq'],
            'name': platform.node()
        }

    def __context_device_get_arch(self):
        if platform.machine() == "AMD64":
            return 'x86_64'
        return 'x86_32'

    def __context_gpu_get(self):
        stats = BigWorld.wg_getClientStatistics()
        return {
            'api_type': 'Direct3D11',
            'name': '%s %s' % (self.__context_gpu_get_vendor_name(stats['gpuVendor']), stats['gpuFamily']),
            'id': '0x%0.4X' % stats['gpuFamily'],
            'memory_size': stats['gpuMemory'],
            'vendor_id': self.__context_gpu_get_vendor(stats['gpuVendor']),
            'vendor_name': self.__context_gpu_get_vendor_name(stats['gpuVendor']),
        }
    
    def __context_gpu_get_vendor(self, vendorid):
        if vendorid == 1: # Intel
            return '0x8086'
        if vendorid == 2: # NVIDIA
            return '0x10DE'
        if vendorid == 3: # AMD
            return '0x1002'
        return '0x0000'

    def __context_gpu_get_vendor_name(self, vendorid):
        if vendorid == 1: # Intel
            return 'Intel'
        if vendorid == 2: # NVIDIA
            return 'NVIDIA'
        if vendorid == 3: # AMD
            return 'AMD'
        return 'Unknown'

    def __context_cpu_get_vendor_name(self, vendorid):
        if vendorid == 1: # Intel
            return 'Intel'
        if vendorid == 2: # NVIDIA
            return 'AMD'
        return 'Unknown'

    def __context_os_get(self):
        return {
            'type'   : 'os',
            'name'   : platform.system(), #TODO: handle Wine, handle Codeweavers PortJump
            'version': platform.version()
        }

    def __context_user_get(self):
        if getPlayerDatabaseID() == 0:
            return None
        return {
            'id'        : str(getPlayerDatabaseID()),
            'username'  : getPlayerName(),
            'ip_address': "{{auto}}"
        }

    #
    # Tags
    #

    def __tags_device_set(self):
        stats = BigWorld.wg_getClientStatistics()
        self.set_tag('cpu.name', stats['cpuName'].strip())
        self.set_tag('cpu.cores', stats['cpuCores'])
        self.set_tag('cpu.freq', stats['cpuFreq'])
        self.set_tag('cpu.vendor', self.__context_cpu_get_vendor_name(stats['cpuVendor']))

    def __tags_storage_set(self):
        stats = BigWorld.wg_getClientStatistics()
        self.set_tag('storage.system', stats['systemHddName'].strip())
        self.set_tag('storage.game', stats['gameHddName'].strip())

    def __tags_modpack_set(self):
        modpacks = list()
        if os.path.exists('ADBokaT57_ModPack'):
            modpacks.append('adbokat57')
        if vfs.file_exists('scripts/client/gui/mods/mod_updater_amway921.pyc'):
            modpacks.append('amway921')
        if vfs.file_exists('scripts/client/gui/mods/mod_aslainsVersionChecker.pyc'):
            modpacks.append('aslain')
        if os.path.exists('champi-wot-modpack-uninstaller.exe'):
            modpacks.append('champi')
        if vfs.file_exists('scripts/client/gui/mods/mod_dmodupdater.pyc'):
            modpacks.append('dmod')
        if vfs.file_exists('scripts/client/gui/mods/mod_modpack.pyc'):
            modpacks.append('jove')
        if vfs.file_exists('scripts/client/gui/mods/mod_korben_notify.pyc'):
            modpacks.append('korben')
        if vfs.file_exists('scripts/client/gui/mods/mod_lebwa_modpack.pyc'):
            modpacks.append('lebwa')
        if vfs.file_exists('scripts/client/gui/mods/mod_pro_stuff.pyc'):
            modpacks.append('protanki')
        if vfs.file_exists('scripts/client/gui/mods/mod_updater_vspishka.pyc'):
            modpacks.append('vspishka')
        if vfs.file_exists('scripts/client/gui/mods/mod_a.pyc'):
            modpacks.append('wotspeak')
        if os.path.exists('res_mods/configs/xvm/3AZHIGALKA'):
            modpacks.append('zazhigalka')

        modpack_id = ",".join(modpacks)
        if not modpack_id:
            modpack_id = 'none'

        self.set_tag('modpack.id', modpack_id)


    #
    # Events
    #

    def __events_subscribe(self):
        dependency.instance(IAppLoader).onGUISpaceEntered += self.__events_onGuiSpaceEntered

    def __events_unsubscribe(self):
        dependency.instance(IAppLoader).onGUISpaceEntered -= self.__events_onGuiSpaceEntered

    def __events_onGuiSpaceEntered(self, spaceID):   
        self.set_user(self.__context_user_get())


    #
    # Public
    #

    def set_context(self, ctx_key, ctx_value):
        if self.__native is not None:
            self.__native.set_context(ctx_key, ctx_value)

        if self.__python is not None:
            self.__python.set_context(ctx_key, ctx_value)


    def set_tag(self, tag_key, tag_value):
        if self.__native is not None:
            self.__native.set_tag(tag_key, tag_value)

        if self.__python is not None:
            self.__python.set_tag(tag_key, tag_value)


    def set_user(self, user_ctx):
        if self.__native is not None:
            self.__native.set_user(user_ctx)

        if self.__python is not None:
            self.__python.set_user(user_ctx)



#
# xfw
#

def xfw_module_init():
    global g_openwg_bugreporter
    g_openwg_bugreporter = OpenWGBugreports()


def xfw_is_module_loaded():
    global g_openwg_bugreporter
    if g_openwg_bugreporter is not None:
        return g_openwg_bugreporter.is_inited()

    return False


def xfw_module_fini():
    global g_openwg_bugreporter
    if g_openwg_bugreporter is not None:
        g_openwg_bugreporter.__fini__()
    g_openwg_bugreporter = None

