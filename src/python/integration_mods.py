#
# Imports
#

# cpython
import importlib
import sys

# sentry_sdk
from sentry_sdk.hub import Hub
from sentry_sdk.integrations import Integration
from sentry_sdk.scope import add_global_event_processor

# xfw.loader
import xfw_loader.python as loader

# openwg.vfs
import openwg_vfs as vfs



#
# Integration
#

class OpenWGModsIntegration(Integration):
    identifier = "openwg_mods"

    def __init__(self):
        self.__installed_mods = dict()

        #xfw
        for key,val in loader.get_mod_ids().items():
            self.__installed_mods[key] = val
        
        #other
        for module_path in sys.modules:
            if module_path.startswith('gui.mods.mod_') and module_path.count('.') == 2:
                if module_path == 'gui.mods.mod_xfw':
                    continue

                module_name = module_path.split('.')[-1]

                module_version = '0.0.0'
                try:
                    module = importlib.import_module(module_path)
                    if module:
                        module_version = getattr(module, '__version__', '0.0.0')
                except:
                    continue

                self.__installed_mods[module_name] = module_version

                

    def get_installed_mods(self):
        return self.__installed_mods


    @staticmethod
    def setup_once():
        @add_global_event_processor
        def processor(event, hint):
            if event.get("type") == "transaction":
                return event

            integration = Hub.current.get_integration(OpenWGModsIntegration)
            if integration is None:
                return event

            event["modules"] = integration.get_installed_mods()
            return event
